import { UserState } from "./user/user.reducer";

export interface IStore {
    user: UserState
}