# Angular ngrx example

During a course on NgRx, I worked on an Angular project that showcase of how NgRx can enhance the state management of an application.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

